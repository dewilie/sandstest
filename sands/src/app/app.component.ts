import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuController,NavController } from 'ionic-angular';

import { LoginPage } from '../pages/login/login';
import { OwnerPage } from '../pages/owner/owner';
import { AddItemPage } from '../pages/add-item/add-item';
import { AddStaffPage } from '../pages/add-staff/add-staff';
import { FinanceReportPage } from '../pages/finance-report/finance-report';
import { OrderPage } from '../pages/order/order';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  addItemPage = AddItemPage;
  addStaffPage = AddStaffPage;
  financeReportPage = FinanceReportPage;
  ownerPage = OwnerPage;
  loginPage = LoginPage;
  orderPage = OrderPage;

  @ViewChild('sideMenuContent') navCtrl: NavController;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen , public menuCtrl: MenuController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  onLoad(page: any) {
    this.navCtrl.setRoot(page);
    this.menuCtrl.close();
  }

}


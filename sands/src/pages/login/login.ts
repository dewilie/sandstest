import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';
import { OwnerPage } from '../owner/owner';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  registerForm: FormGroup;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiProvider
  ) {
  }

  ngOnInit(){
    this.initializeForm();
  }

  private initializeForm(){
    this.registerForm = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  onSubmitForm(){
    console.log(this.registerForm.value);
    let username = this.registerForm.get('username').value;
    let password = this.registerForm.get('password').value;
    // if(username == 'owner'){
    //   this.navCtrl.push(OwnerPage);
    // }
    // else if(username == 'warehouse'){
    //   console.log('Trying to access warehouse menu');
    // }
    // else if(username == 'courier'){
    //   console.log('Trying to access courier menu');
    // }
    let _user = {
      'username': username,
      'password': password
    }
    this.api.login(_user)
    .then((data) =>{
      if(data['status'] == 'success'){
        console.log('Login Sukses');
        this.navCtrl.setRoot(OwnerPage);
        //this.navCtrl.push(OwnerPage);
      }
      else{
        console.log('I love u');
      }
    })
    .catch(() => {
      console.log('Failed Login');
    })
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinanceReportPage } from './finance-report';

@NgModule({
  declarations: [
    FinanceReportPage,
  ],
  imports: [
    IonicPageModule.forChild(FinanceReportPage),
  ],
})
export class FinanceReportPageModule {}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }
  login(user): Promise<any>{
    let url = "http://localhost/api/api_login.php";
    let param = {username: user.username, password: user.password};
  
    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify(param))
      .subscribe(data => {
        resolve(data);
      }, (err) =>{
        console.log('Error broo');
      })
    })
  }
}
